# `KFNB` format

## Header fields

Header format is very similar to RIFF container.

_type_ defines if next data is just number or string.

| name |type| description |
|------|----|-------------|
|`DIFM`| 1 | Karaoke difficulty for men; see [Difficulties](#difficulties) |
|`DIFW`| 1 | Karaoke difficulty for woman; see [Difficulties](#difficulties) |
|`GNRE`| 1 | Song Genre; genre IDs are taken from [ID3 tags](https://en.wikipedia.org/wiki/ID3) |
|`SFTV`| 1 | _&lt;unknown&gt;_ |
|`MUSL`| 1 | Song Length (seconds) |
|`ANME`| 1 | _&lt;unknown&gt;_ (seen values: `12` and `13`) |
|`TYPE`| 1 | _&lt;unknown&gt;_ (always `0`) |
|`FLID`| 2 | 32-bit Encryption key |
|`LANG`| 2 | Song Language (like `EN`) |
|`TITL`| 2 | Song Title |
|`ARTS`| 2 | Song Artist |
|`ALBM`| 2 | Song Album |
|`COMP`| 2 | Song Composer |
|`COPY`| 2 | Copyright |
|`SORC`| 2 | Source File; see [Source File](#source-file) |
|`COMM`| 2 | Comment |
|`YEAR`| 2 | Song Year |
|`TRAK`| 2 | Song Track Number on Album |
|`KARV`| 2 | _KaraFun_ |
|`KFNZ`| 2 | _KarafunizationLbl_ |
|`RGHT`| 1 | _Publishing Rights_ (`0` or `1`) |
|`PROV`| 1 | _&lt;unknown&gt;_ (always `0`) |
|`IDUS`| 2 | _&lt;unknown&gt;_ (always `'                '` - 16 space chars) |
|`ENDH`| 1 | Header end signature (always `0xFFFFFFFF`) |

### Difficulties

|value|description|
|-----|-----------|
| `1` | "_1 - Very easy_" |
| `2` | "_2 - Easy_" |
| `3` | "_3 - Medium_" |
| `4` | "_4 - Hard_" |
| `5` | "_5 - Very hard_" |

others are not shown in KaraFun Player

### Source file

Field value always uses format: "`%d,%c,%s`" (`file_type`, `source_type`, `filename`)

`file_type`:

|value|description|
|-----|-----------|
| `1` | Audio file "_Audio file_" |
| `2` | MIDI/Karaoke file (mid, kar) "_Midi music file_" |
| `6` | Video file "_Video file_" |

`source_type`:

|value|description|
|-----|-----------|
| `I` | Audio source type: "MP3 audio file" without linked file |
| `L` | Audio source type: "MP3 audio file" with linked file |

`filename`:

If `source_type` is `I` then original file name is here.
If `source_type` is `L` then path to audio file is here.

## File directory

File directory starts with 4-byte file count number then followed by all files using following parameters:

|length|description|
|------|-----------|
| `4`  | File name length |
|string| File name (in original encoding, you need to guess it) |
| `4`  | File type; see [File types](#file-types) |
| `4`  | Length (in bytes) |
| `4`  | Offset; where data in container starts (`0` = end of file listing) |
| `4`  | Output length (in bytes) |
| `4`  | Flags /_or just "encrypted"_/ (only `0` and `1` was seen) |

"Length" and "Output length" are the same unless file is encrypted.

### File types

|value|description|
|-----|-----------|
| `1` | Song file |
| `2` | Audio file |
| `3` | Image file |
| `4` | Font file |
| `5` | Video file |

