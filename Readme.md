## KaraFun Specification

This is `*.kfn` file format specification heavily based on very good [reverse-engineering by George from UlduzSoft](https://www.ulduzsoft.com/2012/10/reverse-engineering-the-karafun-file-format-part-1-the-header/).

This is more complete specification; includes more fields information and more stuff found just by messing around with values and checking what KaraFun Player shows in `File Info` windows, checking EXE for similar strings and so on...

KaraFun files are found with few different data inside:
- [`KFN3`](/KFN3.md)
- [ZIP archive](/PKZIP.md)
- [`KFNB`](/KFNB.md)

You may also want to check [`Song.ini`](/Song.ini.md) specification.
