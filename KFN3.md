# `KFN3` format

**This specification is incomplete and is unable to read whole karaoke file!** See end of spec.

This specification is mostly done using reverse-engineering of KaraFun.exe.

After `KFN3` file header you can find fields as follows:

|length|description|
|------|-----------|
| `2`  | Header length (**?**) |
| `2`  | File type |

If "File type" == `1` then as follows:

|length|description|
|------|-----------|
| `64` | Song Title |
| `64` | Song Artist |
| `64` | Song Album |
| `64` | Song Composer |
| `128`| Comment (**?**) |
| `128`| Audio Source |
| `200`| _&lt;unknown&gt;_ |
| `8`  | Song Year |
| `4`  | _&lt;unknown&gt;_ |
| `4`  | _&lt;unknown&gt;_ |
| `8`  | Song Track Number on Album |
| `4`  | _&lt;unknown&gt;_ |
| `2`  | _&lt;unknown&gt;_ |
| `2`  | _&lt;unknown&gt;_ |
| `4`  | _&lt;unknown&gt;_ |
| `4`  | Song Language (like `EN`) |
| `2`  | _&lt;unknown&gt;_ |
| `2`  | _&lt;unknown&gt;_ |
| `2`  | _&lt;unknown&gt;_ |

Now we're at `768` byte of file.

If "Header length" was more than `768` then more fields are available:

|length|description|
|------|-----------|
| `2`  | Duration |
| `64` | _&lt;unknown&gt;_ |
| `64` | _&lt;unknown&gt;_ |
| `64` | _KaraFunization_ |

Now we're at `1024` byte of file.

Unfortunately rest of file is data in _&lt;unknown&gt;_ format.
